# NDNSSEC - Demo
A web-based program to exhibit how NDNSSEC works in the background.

# Requirements
This project depends on the following programs:

  * [`node`](https://nodejs.org/) ( >= `v10 LTS`)
  * [`ndnssec-core`](https://gitlab.com/ndnssec/ndnssec-core)
  * [`ndnssec-dnstools`](https://gitlab.com/ndnssec/ndnssec-dnstools)

Before running the server, makes sure that the `bin` directories of these projects are already on your `PATH`, e.g., by executing:

```bash
export PATH=$PATH:/path/to/ndnssec-core/build/bin:/path/to/ndnssec-dnstools/bin
```

# Quick Start
```bash
# Install dependencies
npm install
# Start server on localhost:8080
npm start
```

# Acknowledgment
This work was supportedin parts by the German Federal Ministry of Education and Research (BMBF) within the projects *I3* and *Deutsches Internet-Institut* (grant no. 16DII111).

---
### Icons Copyright
Copyright 2019 Twitter, Inc and other contributors / Code licensed under the [MIT License](http://opensource.org/licenses/MIT) / Graphics licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/).
