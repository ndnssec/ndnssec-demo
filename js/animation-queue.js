'use strict'

/**
 * @class AsyncQueue
 *
 * A convenient wrapper to allow execution of items
 * in queue synchronously.
 */
export class AnimationQueue extends Array {
  async run () {
    while (this.length) {
      await this.shift()()
    }
  }

  clear () {
    this.length = 0
  }
}
