'use strict'

const errors = require('restify-errors')

const socketTag = 'ndnssec'

/**
 * Handler to initialize NNSSEC for a given identity.
 *
 * @param {callback} cb is called when this handler is called.
 */
function initializeNDNSSECHandler (cb) {
  return async (req, res, next) => {
    const identity = req.body
    try {
      const result = await cb(identity)
      res.json(result)
      return next()
    } catch (err) {
      console.error(err)
      return next(new errors.InternalError('Could not initialize NDNSSEC!'))
    }
  }
}

/**
 * Handler to verify if given producer ID is known and valid.
 *
 * @param {Object} producers list of known producers
 * @returns {Function} generated handler
 */
function verifyProducerIdHandler (producers) {
  return (req, res, next) => {
    if (!(req.params.id in producers)) {
      return new errors.BadRequestError(`'${req.params.id}' is not a valid producer ID!`)
    }
    next()
  }
}

/**
 * Handler which returns the current status of server.
 *
 * @param {Object} producers list of known producers
 * @returns {Function} generated handler
 */
function statusHandler (producers) {
  return (req, res, next) => {
    const result = {}
    Object.keys(producers).forEach(key => { result[key] = (producers[key] !== null) && producers[key].isRunning() })
    res.json(result)
    next()
  }
}

/**
 * Handler to start a producer.
 *
 * @param {Object} producers list of known producers
 * @param {Observer} broadcast function to subscribe to producer's stdout/stderr
 * @return {Function} generated handler
 */
function startProducerHandler (producers, broadcast) {
  return (req, res, next) => {
    const obs = producers[req.params.id].start()
    obs.subscribe(broadcast.bind(undefined, socketTag))
    res.send(200)
    next()
  }
}

/**
 * Handler to stop a producer.
 *
 * @param {Object} producers list of known producers
 * @return {Function} generated handler
 */
function stopProducerHandler (producers) {
  return (req, res, next) => {
    producers[req.params.id].stop()
    res.send(200)
    next()
  }
}

/**
 * Handler to start the consumer (initialized previously)
 * @param {Object} executables containing consumer instant
 * @param {Function} broadcast function to subscribe to producer's stdout/stderr
 */
function startConsumerHandler (executables, broadcast) {
  return (req, res, next) => {
    const type = req.params.type
    if (['normal', 'nonexistent'].indexOf(type) < 0) {
      return next(new errors.BadRequestError(`Type '${type}' is unknown!`))
    }

    const obs = executables.consumers[type].start()
    obs.subscribe(broadcast.bind(undefined, socketTag))
    res.send(200)
    next()
  }
}

module.exports = {
  initializeNDNSSECHandler,
  verifyProducerIdHandler,
  statusHandler,
  startProducerHandler,
  stopProducerHandler,
  startConsumerHandler
}
