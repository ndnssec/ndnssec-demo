/* global rxjs */
'use strict'

const { Subject } = rxjs

const LINE_REGEX = /([\d.]+)\s(\w+:)\s(\[[\w.]+\])\s(\[X\])\s(.+$)/
const ID_REGEX = /.(\w+)\]$/

/**
 * @class LogEntry
 *
 * Represents a parsed log entry from NDNSSEC consumer / producer instances.
 */
class LogEntry {
  /**
   * Constructs.
   *
   * @param {String} entry raw log entry
   */
  constructor (entry) {
    this.date = new Date(+entry[1].split('.')[0] * 1000)
    this.id = entry[3].match(ID_REGEX)[1]
    let cls, type, msg, rest
    [cls, type, msg, ...rest] = entry[5].split(',')
    this.cls = cls
    this.type = type
    this.msg = msg
    this.extra = rest
  }

  /**
   * @returns if entry is of class NDN
   */
  isNDN () {
    return this.cls === LogEntry.CLASS_NDN
  }

  /**
   * @returns if entry is of class DNS
   */
  isDNS () {
    return this.cls === LogEntry.CLASS_DNS
  }

  /**
   * @returns if entry is a query
   */
  isQuery () {
    return this.type === LogEntry.TYPE_QUERY
  }

  /**
   * @returns if entry is an answer
   */
  isAnswer () {
    return this.type === LogEntry.TYPE_ANSWER
  }

  /**
   * @returns if entry is an error
   */
  isError () {
    return this.type === LogEntry.TYPE_ERROR
  }

  /**
   * @returns if entry is a validation entry
   */
  isValidation () {
    return this.type === LogEntry.TYPE_VALIDATION
  }
}
LogEntry.CLASS_NDN = 'NDN'
LogEntry.CLASS_DNS = 'DNS'
LogEntry.TYPE_QUERY = 'Q'
LogEntry.TYPE_ANSWER = 'A'
LogEntry.TYPE_ERROR = 'E'
LogEntry.TYPE_VALIDATION = 'V'

/**
 * @class LogParser
 *
 * Parses law log strings from NDNSSEC consumer / producer instances.
 */
export class LogParser {
  /**
   * Constructs.
   *
   * @param {Observable} logObservable emitting NDNSSEC log lines
   */
  constructor (logObservable) {
    this.parsedObserver = new Subject()
    this.rawObserver = new Subject()
    logObservable.subscribe(this)
  }

  next (lines) {
    // For some reasons we get multiple entries in a single line
    lines.split('\n').filter(line => line.trim() !== '').forEach(line => {
      const match = line.match(LINE_REGEX)
      this.rawObserver.next(line)
      if (match) {
        this.parsedObserver.next(new LogEntry(match))
      }
    })
  }

  error (err) {
    console.error(err)
  }

  complete () {
    // @TODO
  }
}
