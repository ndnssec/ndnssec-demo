/* global $, rxjs, fetch, io */

import { Element, SpeechBubble, Grid, Icons } from './ui.js'
import { LogParser } from './log-parser.js'
import { AnimationQueue } from './animation-queue.js'

const { fromEvent } = rxjs

// IDs of consumer and producers in logs from NDNSSEC
const IDS_CONSUMER = 'Consumer'
const IDS_GOOD = 'NormalProducer'
const IDS_BAD = 'MaliciousProducer'
const IDS_UGLY = 'NonexistentProducer'

// Contains all visual elements
const els = {}

/**
 * Fetches the current status from the server.
 *
 * @returns {Object} status
 */
async function getStatus () {
  const res = await fetch('/status')
  return res.json()
}

/**
 * Default click handler for producer icons.
 *
 * Depending on its current state, a request to
 * start or stop the producer is sent to the server.
 */
async function producerClickHandler () {
  const method = this.state ? 'delete' : 'put'
  const res = await fetch(`/producers/${this.id}`, { method: method })
  if (res.status === 200) {
    const status = await getStatus()
    this.setState(status[this.id])
  } else {
    // @TODO toast the error!
  }
}

function clearPage () {
  if (els.csb) els.csb.hide()
  if (els.answer) els.answer.destroy()
  if (els.dnsQuery) els.dnsQuery.destroy()
}

function fetchElementById (id) {
  return id === IDS_GOOD ? els.good
    : id === IDS_BAD ? els.bad
      : id === IDS_UGLY ? els.ugly
        : id === IDS_CONSUMER ? els.consumer : null
}

/**
 * Starting point of the UI.
 */
async function run () {
  // Load SVG icons
  await Icons.load()

  // Prepare grid
  const grid = new Grid(4, 3)
  grid.draw()

  // Prepare UI
  const iconSize = { w: 50, h: 50 }
  const iconMovableSize = { w: 25, h: 50 }

  // Init form listener
  $('#init > form').submit(async event => {
    event.preventDefault()

    event.target.name.disabled = true
    await fetch('/init', { method: 'put', body: event.target.name.value })

    els.announcement.text('NDNSSEC and DNS initialized // Keys are distributed!')

    $('#init').fadeOut(() => {
      els.pubKey = new Element('pubKey', Icons.key)
        .resize(iconMovableSize)
        .toCenter(grid.centerOf(3, 2))
      els.pubKey._svg.select('path').fill('#009ACD')
      els.pubKey.toCenter(els.dns.west.add(-25, 0), true)

      els.privKey = new Element('privKey', Icons.key)
        .resize(iconMovableSize)
        .toCenter(grid.centerOf(3, 2))
      els.privKey.toCenter(els.good.west.add(-25, 0), true)
    })
  })

  // consumer
  els.consumer = new Element('consumer', Icons.peter)
    .title('Consumer')
    .resize(iconSize)
    .toCenter(grid.centerOf(3, 3))
    .onClick(async function () {
      const type = (els.ugly.state === true) ? 'nonexistent' : 'normal'
      await fetch(`/consumer/${type}`, { method: 'put' })
    })

  // producers
  els.good = new Element('good', Icons.halo)
    .title('Good')
    .resize(iconSize)
    .toCenter(grid.centerOf(2, 1))
    .setState(false)
    .onClick(producerClickHandler)

  els.bad = new Element('bad', Icons.evil)
    .title('Bad')
    .resize(iconSize)
    .toCenter(grid.centerOf(3, 1))
    .setState(false)
    .onClick(producerClickHandler)

  els.ugly = new Element('ugly', Icons.skull)
    .title('Ugly')
    .resize(iconSize)
    .toCenter(grid.centerOf(4, 1))
    .setState(false)
    .onClick(producerClickHandler)

  // dns
  els.dns = new Element('dns', Icons.wizard)
    .title('DNS')
    .resize(iconSize)
    .toCenter(grid.centerOf(1, 2))

  // Speech bubbles for consumer and producers
  els.csb = new SpeechBubble()
  els.psb = new SpeechBubble('', 'left')

  // Announcement
  els.announcement = $('<div id="announcement"></div>')
  $('body').append(els.announcement)
  const announcementPos = grid.posOf(2, 1)
  els.announcement.offset({ top: announcementPos.y, left: announcementPos.x })
  els.announcement.css({ width: grid._tileSize.w, height: grid._tileSize.h, 'line-height': `${grid._tileSize.h}px` })

  const socket = io()
  const socketObserver = fromEvent(socket, 'ndnssec')
  const logParser = new LogParser(socketObserver)
  const animQueue = new AnimationQueue()
  logParser.parsedObserver.subscribe(async entry => {
    if (entry.isNDN() && entry.isQuery()) {
      clearPage()
      animQueue.clear()

      els.csb.update(`Get me ${entry.msg}`)
      els.csb.to(els.consumer.northWest)
      els.csb.show()
    }

    if (entry.isDNS() && entry.isQuery()) {
      animQueue.push(async () => {
        els.announcement.text('DNS query sent...')
        if (els.dnsQuery) els.dnsQuery.destroy()
        els.dnsQuery = new Element('dnsQuery', Icons.envelope)
          .resize(iconMovableSize)
          .toCenter(els.consumer.northWest.add(-25, -25))
        await els.dnsQuery.toCenter(els.dns.east.add(25, 0), true)
      })
    }

    if (entry.isNDN() && entry.isAnswer()) {
      const source = fetchElementById(entry.id)
      animQueue.push(async () => {
        els.announcement.text('NDN response on the way...')
        if (els.answer) els.answer.destroy()
        els.answer = new Element('answer', Icons.package)
          .resize(iconMovableSize)
          .toCenter(source.east.add(25, 0))
        await els.answer.toCenter(els.consumer.west.add(-25, 0), true)
      })
    }

    if (entry.isNDN() && entry.isValidation()) {
      animQueue.push(async () => {
        await els.dnsQuery.toCenter(els.consumer.northWest.add(-25, -25), true)
      })
      animQueue.push(async () => {
        els.announcement.text('Packet validation in progess...')
        els.csb.update(entry.extra)
        els.csb.show()
        els.csb.to(els.consumer.northWest)
      })

      animQueue.run()
    }

    if (entry.isNDN() && entry.isError()) {
      animQueue.push(() => {
        const el = fetchElementById(entry.id)
        const sb = el === els.consumer ? els.csb : els.psb
        sb.update(entry.extra.length === 0 ? entry.msg : entry.extra)
        sb.show()
        sb.to(el === els.consumer ? el.northWest : el.northEast)
      })

      animQueue.run()
    }
  })

  const terminal = $('#terminal')
  logParser.rawObserver.subscribe(val => {
    terminal.append($(`<div>${val}</div>`))
    terminal.scrollTop(terminal.prop('scrollHeight'))
  })
}

run()
