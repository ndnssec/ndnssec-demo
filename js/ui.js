/* global $, SVG, fetch */
'use strict'

// Container DIV for all element drawings
const canvasId = 'drawing'

/**
 * @class Position
 *
 * Represents a 2D point with some convenient
 * manipulation methods.
 */
export class Position {
  /**
   * Constructs.
   *
   * @param {Number} x initial x
   * @param {Number} y initial y
   */
  constructor (x = 0, y = 0) {
    this.x = x
    this.y = y
  }

  /**
   * Cartesian addition.
   *
   * @param {Number} x
   * @param {Number} y
   */
  add (x = 0, y = 0) {
    this.x += x
    this.y += y
    return this
  }
}

/**
 * @class Element
 *
 * Wraps an SVG element and provides convenient
 * methods for positioning, scaling and event
 * handling.
 */
export class Element {
  /**
   * Constructs.
   *
   * @param {String} id DIV ID for created element
   * @param {String} icon SVG icon as string
   */
  constructor (id, icon) {
    this.id = id
    this._el = $(`<div id="${id}" class="icon"></div>`)
    $(`#${canvasId}`).append(this._el)
    this._svg = new SVG(id).svg(icon)
    this._active = true
    this._animationDuration = 2000
  }

  /**
   * Removes Element from the DOM.
   */
  destroy () {
    this._el.remove()
  }

  /**
   * Resizes the element.
   *
   * @param {Object} dimension object with w (width) and (height) keys
   * @returns {Element} this
   */
  resize ({ w, h } = { w: 50, h: 50 }) {
    this._el.width(w)
    this._el.height(h)
    return this
  }

  /**
   * Adds a title text to Element (below).
   *
   * @param {String} text desired title
   */
  title (text) {
    const titelEl = $(`<div class="title">${text}</div>`)
    this._el.append(titelEl)
    return this
  }

  /**
   * Returns absolute position of the element (its center).
   *
   * @returns {Position} center of Element
   */
  get center () {
    return new Position(
      (this._el.offset().left + (this._el.width() / 2)),
      (this._el.offset().top + (this._el.height() / 2))
    )
  }

  /**
   * @returns {Position} north center of Element
   */
  get north () {
    return new Position(
      (this._el.offset().left + (this._el.width() / 2)),
      (this._el.offset().top)
    )
  }

  /**
   * @returns {Position} north west of Element
   */
  get northWest () {
    return new Position(
      this._el.offset().left,
      this._el.offset().top
    )
  }

  /**
   * @returns {Position} north east of Element
   */
  get northEast () {
    return new Position(
      this._el.offset().left + this._el.width(),
      this._el.offset().top
    )
  }

  /**
   * @returns {Position} east of Element
   */
  get east () {
    return new Position(
      this._el.offset().left + this._el.width(),
      (this._el.offset().top) + (this._el.height() / 2)
    )
  }

  /**
   * @returns {Position} west of Element
   */
  get west () {
    return new Position(
      this._el.offset().left,
      (this._el.offset().top) + (this._el.height() / 2)
    )
  }

  /**
   * @returns {Position} south of Element
   */
  get south () {
    return new Position(
      this._el.offset().left + (this._el.width() / 2),
      this._el.offset().top + this._el.height()
    )
  }

  /**
   * Moves the element by its center to given position.
   *
   * @param {Object} position object with x and y keys
   * @param {Boolean} animate true to animate the movement
   * @returns {Element|Promise} this or promise if animation set to true
   */
  toCenter ({ x, y } = { x: 0, y: 0 }, animate = false) {
    const target = {
      top: (y - (this._el.height() / 2)),
      left: (x - (this._el.width() / 2))
    }
    if (animate) {
      return new Promise((resolve, reject) => {
        this._el.animate(target, this._animationDuration, resolve)
      })
    } else {
      this._el.css(target)
      return this
    }
  }

  /**
   * @returns {Boolean} the current state (active or not)
   */
  get state () {
    return this._active
  }

  /**
   * Set's element state (active or inactive).
   *
   * @param {Boolean} active true to activate
   * @returns {Element} this
   */
  setState (active = true) {
    this._active = active
    this._el.toggleClass('inactive', !this._active)
    return this
  }

  /**
   * Toggles element's state.
   *
   * @return {Element} this
   */
  toggleState () {
    return this.setState(!this._active)
  }

  /**
   * Registers a click handler for the element.
   *
   * @param {Function} handler click handler
   * @returns {Element} this
   */
  onClick (handler) {
    this._el.on('click', handler.bind(this))
    return this
  }
}

/**
 * @class SpeechBubble
 */
export class SpeechBubble {
  constructor (text = '', orientation = 'right') {
    this._el = $(`<div class="speech-bubble"><p>${text}</p></div>`)
    this._orientation = orientation
    if (orientation === 'left') this._el.addClass('left')
    this._el.hide()
    $(`#${canvasId}`).append(this._el)
  }

  /**
   * Moves speech bubble's tip to given position.
   *
   * @param {Position} pos desired position
   * @returns this speech bubble
   */
  to ({ x, y }) {
    this._el.css({
      top: y - this._el.outerHeight(true),
      left: x + ((this._orientation === 'right' ? -1 * this._el.width() : 0))
    })
    return this
  }

  /**
   * Show's bubble.
   *
   * @param {Number} duration visibility duration (equals 5000 ms)
   * @returns this speech bubble
   */
  show (duration = 5000) {
    // Reset any previous timer
    if (this.timerID) clearTimeout(this.timerID)
    this._el.show()
    // Somehow jQuery delay() is no working...
    this.timerID = setTimeout(() => this.hide(), duration)
    return this
  }

  /**
   * Hides speech bubble.
   *
   * @returns this speech bubble
   */
  hide () {
    this._el.hide()
    return this
  }

  /**
   * Updates bubble's text.
   *
   * @param {String} text
   * @returns this speech bubble
   */
  update (text) {
    this._el.children('p').text(text)
    return this
  }
}

/**
 * @class Grid
 *
 * Convenient class to generate a flexible grid
 * for a given DIV.
 */
export class Grid {
  /**
   * Constructs.
   *
   * @param {Number} rows desired number of rows
   * @param {Number} cols desired number of columns
   */
  constructor (rows = 3, cols = 3) {
    this._rows = rows
    this._cols = cols
    this._tileSize = { h: null, w: null }
    this._size = { h: null, w: null }
    this._gridHelperClass = 'grid-helper'
    this.setupSize()
  }

  /**
   * Initilizes available size.
   */
  setupSize () {
    window.onresize = this.setupSize.bind(this)
    const canvas = document.getElementById(canvasId)
    this._size = {
      w: canvas.clientWidth,
      h: canvas.clientHeight
    }

    this._tileSize = {
      w: this._cols === 1 ? this._size.w : (this._size.w / this._cols),
      h: this._rows === 1 ? this._size.h : (this._size.h / this._rows)
    }
  }

  /**
   * Calculates the top left position of a tile in the grid.
   *
   * @param {Number} row desired row
   * @param {Number} col desired column
   * @returns {Position} top left position of the matching tile in the grid
   */
  posOf (row = 1, col = 1) {
    if (row < 0 || row > this._rows || col < 0 || col > this._cols) {
      throw new Error(`Invalid row '${row}' or column '${col}' (grid: ${this._rows} x ${this._cols})`)
    }

    return new Position(
      (this._tileSize.w * col),
      (this._tileSize.h * row)
    )
  }

  /**
   * Calculates the center of a tile in the grid.
   *
   * @param {Number} row desired row
   * @param {Number} col desired column
   * @returns {Position} center of the matching tile in the grid
   */
  centerOf (row = 1, col = 1) {
    return this.posOf(row, col).add(
      -(this._tileSize.w / 2),
      -(this._tileSize.h / 2)
    )
  }

  /**
   * Draws grid helper lines.
   */
  draw () {
    const canvas = $(`#${canvasId}`)
    const baseEl = $(`<div class="${this._gridHelperClass}"></div>`)

    for (let row = 0; row < this._rows; row++) {
      const defaultStyle = { position: 'absolute', left: 0, 'border-bottom': '1px dashed lightgray' }
      const r = baseEl.clone()
      r.css({ ...defaultStyle, width: this._size.w, height: this._tileSize.h, top: row * this._tileSize.h })
      canvas.append(r)
    }
    for (let col = 0; col < this._cols; col++) {
      const defaultStyle = { position: 'absolute', top: 0, 'border-right': '1px dashed lightgray' }
      const c = baseEl.clone()
      c.css({ ...defaultStyle, height: this._size.h, width: this._tileSize.w, left: col * this._tileSize.w })
      canvas.append(c)
    }
  }
}

/**
 * @class Icons
 *
 * Convenient class to handle svg icons.
 */
export class Icons {
  /**
   * Loads predefined icons.
   */
  static async load () {
    const names = ['cloud', 'computer', 'laptop', 'server', 'smartphone', 'halo', 'evil', 'skull', 'peter', 'envelope', 'wizard', 'hirn', 'key', 'package']
    for (const name of names) {
      const res = await fetch(`/images/${name}.svg`)
      const rawSvg = await res.text()
      Icons[name] = rawSvg
    }
  }
}
