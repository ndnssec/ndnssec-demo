'use strict'

const { execSync } = require('child_process')
const { resolve } = require('path')
const { realpathSync, readFileSync, writeFileSync } = require('fs')
const { randomBytes } = require('crypto')

const { render } = require('mustache')

const TEMPLATES_BASE = resolve(__dirname, '..', 'templates')
const ZONE_TEMPLATE_PATH = resolve(TEMPLATES_BASE, 'zone')
const NSD_CONF_NAME = 'nsd.conf'
const NSD_TEMPLATE_PATH = resolve(TEMPLATES_BASE, NSD_CONF_NAME)
const SIGN_ALG = 'RSASHA1-NSEC3-SHA1'
const ZSK_SIZE = 1024
const KSK_SIZE = 2048

/**
 * Synchronously executes command and returns the stdout/strerr.
 *
 * @param {String} cmd command to run
 * @param {Object} options to pass to fs.execSync
 * @returns {String} processes stdout/stderr
 */
function exec (cmd, options) {
  return execSync(cmd, options).toString('utf8').replace(/\n$/, '')
}

/**
 * @class NSD
 *
 * Convenient wrapper to configure and start/stop NSD using Docker.
 *
 * NOTE: All zone and configuration files are generated inside a temp directory.
 */
class NSD {
  /**
   * Constructs.
   *
   * @param {String} namePrefix NDN name prefix (translated into domain)
   */
  constructor (namePrefix) {
    this.domain = namePrefix.split('/').reverse().join('.')
    this.cwd = process.cwd()
    this.paths = null
    this.containerID = null
  }

  /**
   * Synchronously executes a command.
   *
   * @param {String} cmd desired command to execute.
   * @returns {String}
   */
  exec (cmd) {
    return exec(cmd, { cwd: this.cwd })
  }

  /**
   * Initializes NSD.
   *
   * - Create zone file from template
   * - Create ZSK and KSK
   * - Sign zone file
   * - Create NSD config
   *
   * @returns {Object} object containing paths to created files
   */
  init () {
    const result = {}

    // Create a temp directory
    this.cwd = realpathSync(this.exec('mktemp -d'))

    // Generate a zone file
    const zoneTemplate = readFileSync(ZONE_TEMPLATE_PATH)
    const zoneFile = render(zoneTemplate.toString('utf8'), { domain: this.domain })
    const zoneFilePath = resolve(this.cwd, `${this.domain}zone`)
    writeFileSync(zoneFilePath, zoneFile)
    result.zone = zoneFilePath

    // Generate ZSK
    const zsk = this.exec(`ldns-keygen -k -a ${SIGN_ALG} -b ${ZSK_SIZE} ${this.domain}`)
    result.zsk = resolve(this.cwd, zsk)

    // Generate KSK
    const ksk = this.exec(`ldns-keygen -k -a ${SIGN_ALG} -b ${KSK_SIZE} ${this.domain}`)
    result.ksk = resolve(this.cwd, ksk)

    // Generate salt and sign zone
    const salt = randomBytes(16).toString('hex')
    this.exec(`ldns-signzone -n -p -s "${salt}" "${this.domain}zone" ${zsk} ${ksk}`)
    result.zoneSigned = `${zoneFilePath}.signed`

    // Generate NSD config
    const confTemplate = readFileSync(NSD_TEMPLATE_PATH)
    const confFile = render(confTemplate.toString('utf8'), { domain: this.domain })
    const confFilePath = resolve(this.cwd, NSD_CONF_NAME)
    writeFileSync(confFilePath, confFile)
    result.conf = confFilePath

    this.paths = result
    return this.paths
  }

  start () {
    if (this.paths === null) {
      throw new Error('NSD config have not been initialized!')
    }

    this.containerID = this.exec([
      'docker run',
      '-d',
      '-p 127.0.0.1:53:53',
      '-p 127.0.0.1:53:53/udp',
      `-v ${this.cwd}:/etc/nsd:ro`,
      '--name nsd',
      'nsd'
    ].join(' '))
  }

  stop () {
    // Just in case, we will kill both using container
    // ID and name (nsd)
    try {
      this.exec('docker rm -f nsd')
    } catch (_err) {
      // NoOp if nsd is not running!
    }
    if (this.containerID !== null) {
      this.exec(`docker rm -f ${this.containerID}`)
    }
  }
}

/**
 * Imports key from given path in NDN.
 *
 * @param {String} identity
 * @param {String} keyPath
 * @returns {String} result from execution
 */
function importKey (identity, keyPath) {
  try {
    execSync(`ndnsec-delete ${identity}`, { stdio: 'ignore' })
  } catch (_err) {
    // NoOp if identity does not exist!
  }
  return execSync(`createcert ${identity} ${keyPath} | ndnsec-import -P test -`)
}

function setupNonExistent (identity) {
  try {
    execSync(`ndnsec-keygen ${identity}/nonexistent`)
  } catch (_err) {
    // NoOp if identity alreday exists!
  }
}

module.exports = { NSD, importKey, setupNonExistent }
