'use strict'

const { spawn } = require('child_process')
const { fromEvent, merge } = require('rxjs')

// Common options
const execOpts = { env: { ...process.env, NDN_LOG: 'de.*=TRACE' } }

/**
 * @class Executable
 *
 * A convenient class to manage executables
 * outside nodejs runtime.
 */
class Executable {
  /**
   * constructs
   * @param  {...any} arguments same as those passed to child_process.spawn
   */
  constructor (...args) {
    this._args = args
    this._process = null
  }

  /**
   * Kills the executable.
   */
  stop () {
    if (this._process !== null) {
      this._process.kill()
      this._process = null
    }
  }

  /**
   * Starts the executable.
   *
   * @returns {Observable} an observable of the process stdout and stderr
   */
  start () {
    this.stop()

    this._process = spawn(...this._args)
    const stdoutObservable = fromEvent(this._process.stdout, 'data')
    const errorObservable = fromEvent(this._process.stderr, 'data')
    return merge(stdoutObservable, errorObservable)
  }

  /**
   * @returns {Boolean} trus if process is running.
   */
  isRunning () {
    return this._process !== null
  }
}

class GoodProducer extends Executable {
  constructor (identity) {
    super('producer', [identity], execOpts)
  }
}

class BadProducer extends Executable {
  constructor (identity) {
    super('producer-malicious', [identity], execOpts)
  }
}

class UglyProducer extends Executable {
  constructor (identity) {
    super('producer-nonexistent', [identity], execOpts)
  }
}

class Consumer extends Executable {
  constructor (name, keyPath, resolvconfPath) {
    super('consumer', [name, keyPath, resolvconfPath], execOpts)
  }
}

module.exports = { GoodProducer, BadProducer, UglyProducer, Consumer }
