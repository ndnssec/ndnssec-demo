'use strict'

const restify = require('restify')
const socketio = require('socket.io')
const { resolve } = require('path')

const { NSD, importKey, setupNonExistent } = require('./js/nsd')
const { GoodProducer, BadProducer, UglyProducer, Consumer } = require('./js/executable')
const { initializeNDNSSECHandler, verifyProducerIdHandler, statusHandler, startProducerHandler, stopProducerHandler, startConsumerHandler } = require('./js/handlers')

// Consumer options
const resolvconfPath = resolve(`${__dirname}/config/resolv.conf`)

// Prepare HTTP and WS server
const server = restify.createServer()
const io = socketio.listen(server.server)

const executables = {
  producers: {
    good: null,
    bad: null,
    ugly: null
  },
  consumers: {
    normal: null,
    nonexistent: null
  }
}

/**
 * Broadcasts the given tag/payload on web socket.
 *
 * @param {String} tag
 * @param {String} payload
 */
function broadcast (tag, payload) {
  io.sockets.emit(tag, payload.toString('utf8'))
}

/**
 * Async function which succeeds after server is connected.
 *
 * @param {Object} server restify server to start
 * @returns {Promise<Object>} started restify server
 */
function connectServer (server) {
  return new Promise((resolve, reject) => {
    server.listen(8080, () => { resolve(server) })
  })
}

/**
 * Initializes NDNSSEC for a give identity.
 *
 * First zone files and config files for NSD are initialized
 * and nsd is started, then keys are imported in local keychain,
 * and finally producers and the consumer are created.
 *
 * @param {string} identity desired identity
 * @returns {Object} object containing paths to generated keys
 */
function initializeCallback (identity) {
  const nsd = new NSD(identity)

  // Stop everything that might still be running
  nsd.stop()
  Object.keys(executables.producers).map(p => {
    if (executables.producers[p] !== null) executables.producers[p].stop()
  })
  Object.keys(executables.consumers).map(c => {
    if (executables.consumers[c] !== null) executables.consumers[c].stop()
  })

  const result = nsd.init()
  nsd.start()
  importKey(identity, result.ksk)
  setupNonExistent(identity)

  executables.producers.good = new GoodProducer(identity)
  executables.producers.bad = new BadProducer(identity)
  executables.producers.ugly = new UglyProducer(identity)
  executables.consumers.normal = new Consumer(identity, `${result.ksk}.key`, resolvconfPath)
  executables.consumers.nonexistent = new Consumer(`${identity}/nonexistent`, `${result.ksk}.key`, resolvconfPath)
  return result
}

/**
 * Sets up REST routes.
 *
 * @param {Object} server restify server
 */
function setupRoutes (server) {
  server.get('/*', restify.plugins.serveStatic({
    directory: __dirname,
    default: 'index.html'
  }))
  server.put('/init', [restify.plugins.bodyReader(), initializeNDNSSECHandler(initializeCallback)])
  server.get('/status', statusHandler(executables.producers))
  // Producers
  server.put('/producers/:id', [
    verifyProducerIdHandler(executables.producers),
    startProducerHandler(executables.producers, broadcast)
  ])
  server.del('/producers/:id', [
    verifyProducerIdHandler(executables.producers),
    stopProducerHandler(executables.producers)
  ])
  // Consumer
  server.put('/consumer/:type', startConsumerHandler(executables, broadcast))
}

/**
 * Starts and sets up the server
 */
async function start () {
  setupRoutes(server)
  await connectServer(server)
}

start()
